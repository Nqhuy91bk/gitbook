

# Summary
* [Part I](chapter-1/README.md)
* [Writing is nice](chapter-1/README.md#writing)
* [GitBook is nice](chapter-1/something.md#gitbook)
* [Part II](chapter-2/README.md)
* [We love feedback](chapter-2/README.md#feedback)
* [Better tools for authors](chapter-2/something.md#useful-tools)

